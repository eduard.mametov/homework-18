#include <iostream>
using namespace std;

class Stack
{
public:

	Stack()
	{
		top = 0;
		size = 0;
	}

	void Push(int value)
	{	
		if (top >= size)
		{
			int* temp = stack;
			size += 5;
			stack = new int[size + 1];
			for (int i = 0; i < top; i++)
			{
				stack[i] = temp[i];
			}
			delete[] temp;
		}

		stack[top++] = value;
	}


	int Pop()
	{
		if (top == 0)
		{
			return 0;
		}
		--top;
		return stack[top];
	}

	~Stack()
	{
		if (size > 0)
		{
			delete[] stack;
		}
	}

	
	int Size()
	{
		return size;
	}
	
	void Print()
	{
		int* p;

		p = stack;

		cout << "Stack: " << endl;
		
		if (top == 0) 
		{
			cout << "������." << endl;
		}

		for (int i = 0; i < top; i++)
		{
			cout << "�������[" << i << "] = " << *p << endl;
			p++;
		}
		cout << endl;
	}

private:
	int top;
	int size;
	int* stack;

};

int main()
{
	setlocale(LC_ALL, "Russian");

	Stack st1;

	st1.Push(1);
	st1.Push(2);
	st1.Push(3);
	st1.Push(4);
	st1.Push(5);

	st1.Print();

	cout << "������� �������: " << st1.Pop() << endl;
	cout << "������� �������: " << st1.Pop() << endl;
	
	st1.Print(); 

	st1.Push(4);
	st1.Push(5);
	st1.Push(6);
	st1.Push(7);
	st1.Push(8);
	st1.Push(9);
	st1.Push(10);
	st1.Push(11);
	st1.Push(12);
	st1.Push(13);
	st1.Push(14);
	st1.Push(15);
	st1.Push(16);
	st1.Push(17);
	
	st1.Print();

	cout << "������� �������: " << st1.Pop() << endl;
	cout << "������� �������: " << st1.Pop() << endl;
	cout << "������� �������: " << st1.Pop() << endl;
	cout << "������� �������: " << st1.Pop() << endl;
	cout << "������� �������: " << st1.Pop() << endl;
	st1.Print();

	st1.Push(13);
	st1.Push(14);
	st1.Push(15);
	st1.Push(16);
	st1.Push(17);
	st1.Push(18);
	st1.Push(19);
	st1.Push(20);
	st1.Push(21);
	st1.Push(22);
	st1.Push(23);


	st1.Print();
	return 0;
}
